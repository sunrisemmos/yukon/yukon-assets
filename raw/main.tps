<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>5.4.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser-rounded-pivot-points</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">PngQuantLow</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Good</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>json</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>main.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <true/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">main/action-icon.png</key>
            <key type="filename">main/emotes/29-small.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,10,17,19</rect>
                <key>scale9Paddings</key>
                <rect>8,10,17,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/actions.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,99,70,198</rect>
                <key>scale9Paddings</key>
                <rect>35,99,70,198</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/badge-free.png</key>
            <key type="filename">main/badge-member.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,8,17,17</rect>
                <key>scale9Paddings</key>
                <rect>9,8,17,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/balloon-emote.png</key>
            <key type="filename">main/balloon-text.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,10,65,20</rect>
                <key>scale9Paddings</key>
                <rect>32,10,65,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/balloon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,17,65,34</rect>
                <key>scale9Paddings</key>
                <rect>32,17,65,34</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/blue-arrow.png</key>
            <key type="filename">main/blue-x.png</key>
            <key type="filename">main/buddies-icon-disabled.png</key>
            <key type="filename">main/buddies-icon.png</key>
            <key type="filename">main/buddies-remove-icon.png</key>
            <key type="filename">main/emote-icon.png</key>
            <key type="filename">main/grey-arrow.png</key>
            <key type="filename">main/grey-x.png</key>
            <key type="filename">main/help-icon-disabled.png</key>
            <key type="filename">main/help-icon.png</key>
            <key type="filename">main/ignore-icon-disabled.png</key>
            <key type="filename">main/ignore-icon.png</key>
            <key type="filename">main/ignore-remove-icon.png</key>
            <key type="filename">main/mute-icon.png</key>
            <key type="filename">main/online-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/blue-button-active.png</key>
            <key type="filename">main/blue-button-disabled.png</key>
            <key type="filename">main/blue-button-hover.png</key>
            <key type="filename">main/blue-button.png</key>
            <key type="filename">main/grey-button-active.png</key>
            <key type="filename">main/grey-button-hover.png</key>
            <key type="filename">main/grey-button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,14,28,28</rect>
                <key>scale9Paddings</key>
                <rect>14,14,28,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/buddy-button-hover.png</key>
            <key type="filename">main/buddy-button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,18,45,36</rect>
                <key>scale9Paddings</key>
                <rect>23,18,45,36</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/buddy-online-large.png</key>
            <key type="filename">main/buddy-online.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>98,19,196,37</rect>
                <key>scale9Paddings</key>
                <rect>98,19,196,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/buddy/icon-ignore.png</key>
            <key type="filename">main/buddy/icon-load.png</key>
            <key type="filename">main/buddy/icon-mascot.png</key>
            <key type="filename">main/buddy/icon-none.png</key>
            <key type="filename">main/buddy/icon-offline.png</key>
            <key type="filename">main/buddy/icon-online.png</key>
            <key type="filename">main/buddy/icon-player.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,10,21,20</rect>
                <key>scale9Paddings</key>
                <rect>11,10,21,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/buddy/item-hover.png</key>
            <key type="filename">main/buddy/item.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>85,12,170,24</rect>
                <key>scale9Paddings</key>
                <rect>85,12,170,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/buddy/scroll.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,94,26,188</rect>
                <key>scale9Paddings</key>
                <rect>13,94,26,188</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/card-badge-free.png</key>
            <key type="filename">main/card-badge-member.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,20,47,41</rect>
                <key>scale9Paddings</key>
                <rect>24,20,47,41</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/card-bg-player.png</key>
            <key type="filename">main/card-bg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>116,162,232,324</rect>
                <key>scale9Paddings</key>
                <rect>116,162,232,324</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/card-coin.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,11,17,22</rect>
                <key>scale9Paddings</key>
                <rect>8,11,17,22</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/card-photo.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>103,102,205,203</rect>
                <key>scale9Paddings</key>
                <rect>103,102,205,203</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/chat-box.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>143,14,285,27</rect>
                <key>scale9Paddings</key>
                <rect>143,14,285,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/chat-icon.png</key>
            <key type="filename">main/gift-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,15</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/chatlog/bg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>206,219,412,438</rect>
                <key>scale9Paddings</key>
                <rect>206,219,412,438</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/chatlog/message-hover.png</key>
            <key type="filename">main/chatlog/message.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>190,12,380,23</rect>
                <key>scale9Paddings</key>
                <rect>190,12,380,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/crosshair.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>42,42,84,84</rect>
                <key>scale9Paddings</key>
                <rect>42,42,84,84</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/dance.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,24,33,49</rect>
                <key>scale9Paddings</key>
                <rect>16,24,33,49</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/dock.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>286,23,572,45</rect>
                <key>scale9Paddings</key>
                <rect>286,23,572,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/edit_player_button-active.png</key>
            <key type="filename">main/edit_player_button-hover.png</key>
            <key type="filename">main/edit_player_button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>22,16,43,32</rect>
                <key>scale9Paddings</key>
                <rect>22,16,43,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>51,115,101,229</rect>
                <key>scale9Paddings</key>
                <rect>51,115,101,229</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/1-small.png</key>
            <key type="filename">main/emotes/10-small.png</key>
            <key type="filename">main/emotes/11-small.png</key>
            <key type="filename">main/emotes/2-small.png</key>
            <key type="filename">main/emotes/3-small.png</key>
            <key type="filename">main/emotes/30-small.png</key>
            <key type="filename">main/emotes/4-small.png</key>
            <key type="filename">main/emotes/5-small.png</key>
            <key type="filename">main/emotes/6-small.png</key>
            <key type="filename">main/emotes/7-small.png</key>
            <key type="filename">main/emotes/8-small.png</key>
            <key type="filename">main/emotes/9-small.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,10,19,19</rect>
                <key>scale9Paddings</key>
                <rect>10,10,19,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/1.png</key>
            <key type="filename">main/emotes/10.png</key>
            <key type="filename">main/emotes/11.png</key>
            <key type="filename">main/emotes/2.png</key>
            <key type="filename">main/emotes/3.png</key>
            <key type="filename">main/emotes/4.png</key>
            <key type="filename">main/emotes/5.png</key>
            <key type="filename">main/emotes/6.png</key>
            <key type="filename">main/emotes/7.png</key>
            <key type="filename">main/emotes/8.png</key>
            <key type="filename">main/emotes/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,12,23,23</rect>
                <key>scale9Paddings</key>
                <rect>12,12,23,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/12-small.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,11,15,21</rect>
                <key>scale9Paddings</key>
                <rect>8,11,15,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/12.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,13,18,25</rect>
                <key>scale9Paddings</key>
                <rect>9,13,18,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/13-small.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,9,21,18</rect>
                <key>scale9Paddings</key>
                <rect>10,9,21,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/13.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,12,26,23</rect>
                <key>scale9Paddings</key>
                <rect>13,12,26,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/14.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,9,13,18</rect>
                <key>scale9Paddings</key>
                <rect>7,9,13,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/15.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>2,9,4,18</rect>
                <key>scale9Paddings</key>
                <rect>2,9,4,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/16-small.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,11,25,21</rect>
                <key>scale9Paddings</key>
                <rect>13,11,25,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/16.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,13,31,26</rect>
                <key>scale9Paddings</key>
                <rect>16,13,31,26</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/17-small.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,11,19,23</rect>
                <key>scale9Paddings</key>
                <rect>10,11,19,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/17.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,14,23,27</rect>
                <key>scale9Paddings</key>
                <rect>12,14,23,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/18-small.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,11,19,21</rect>
                <key>scale9Paddings</key>
                <rect>10,11,19,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/18.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,13,23,25</rect>
                <key>scale9Paddings</key>
                <rect>12,13,23,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/19.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,10,15,21</rect>
                <key>scale9Paddings</key>
                <rect>8,10,15,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/20.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,9,24,18</rect>
                <key>scale9Paddings</key>
                <rect>12,9,24,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/21.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,13,29,25</rect>
                <key>scale9Paddings</key>
                <rect>15,13,29,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/22.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,13,27,27</rect>
                <key>scale9Paddings</key>
                <rect>13,13,27,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/23.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,12,23,23</rect>
                <key>scale9Paddings</key>
                <rect>11,12,23,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/24-small.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,8,20,16</rect>
                <key>scale9Paddings</key>
                <rect>10,8,20,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/24.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,10,25,19</rect>
                <key>scale9Paddings</key>
                <rect>13,10,25,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/25.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,13,27,26</rect>
                <key>scale9Paddings</key>
                <rect>14,13,27,26</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/26-small.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,11,17,21</rect>
                <key>scale9Paddings</key>
                <rect>8,11,17,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/26.png</key>
            <key type="filename">main/emotes/27.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,13,20,25</rect>
                <key>scale9Paddings</key>
                <rect>10,13,20,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/28-small.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,11,19,23</rect>
                <key>scale9Paddings</key>
                <rect>9,11,19,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/28.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,14,23,27</rect>
                <key>scale9Paddings</key>
                <rect>11,14,23,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/29.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,15,25,29</rect>
                <key>scale9Paddings</key>
                <rect>12,15,25,29</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/emotes/30.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,12,24,24</rect>
                <key>scale9Paddings</key>
                <rect>12,12,24,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/help-button-active.png</key>
            <key type="filename">main/help-button-hover.png</key>
            <key type="filename">main/help-button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>140,26,279,53</rect>
                <key>scale9Paddings</key>
                <rect>140,26,279,53</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/hint.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>44,11,88,22</rect>
                <key>scale9Paddings</key>
                <rect>44,11,88,22</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/igloo-icon-disabled.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,9,19,18</rect>
                <key>scale9Paddings</key>
                <rect>9,9,19,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/igloo-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,9,19,19</rect>
                <key>scale9Paddings</key>
                <rect>10,9,19,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/inventory/bg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>184,162,367,325</rect>
                <key>scale9Paddings</key>
                <rect>184,162,367,325</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/inventory/scroll.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,120,26,239</rect>
                <key>scale9Paddings</key>
                <rect>13,120,26,239</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/inventory/sort-button-hover.png</key>
            <key type="filename">main/inventory/sort-button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>74,12,147,24</rect>
                <key>scale9Paddings</key>
                <rect>74,12,147,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/large-box-empty.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>30,30,60,60</rect>
                <key>scale9Paddings</key>
                <rect>30,30,60,60</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/large-box-hover.png</key>
            <key type="filename">main/large-box.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>31,31,62,62</rect>
                <key>scale9Paddings</key>
                <rect>31,31,62,62</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/list/small-hover.png</key>
            <key type="filename">main/list/small.png</key>
            <key type="filename">main/list/small_arrow-hover.png</key>
            <key type="filename">main/list/small_arrow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>67,17,134,34</rect>
                <key>scale9Paddings</key>
                <rect>67,17,134,34</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/list/wide-hover.png</key>
            <key type="filename">main/list/wide.png</key>
            <key type="filename">main/list/wide_arrow-hover.png</key>
            <key type="filename">main/list/wide_arrow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>133,17,266,34</rect>
                <key>scale9Paddings</key>
                <rect>133,17,266,34</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/mail-button-hover.png</key>
            <key type="filename">main/mail-button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,25,45,49</rect>
                <key>scale9Paddings</key>
                <rect>23,25,45,49</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/mail-icon-disabled.png</key>
            <key type="filename">main/mail-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,6,15,12</rect>
                <key>scale9Paddings</key>
                <rect>8,6,15,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/mail-new.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,10,20,20</rect>
                <key>scale9Paddings</key>
                <rect>10,10,20,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/map-button-hover.png</key>
            <key type="filename">main/map-button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>28,28,56,56</rect>
                <key>scale9Paddings</key>
                <rect>28,28,56,56</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/mod-icon-disabled.png</key>
            <key type="filename">main/mod-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,15,15</rect>
                <key>scale9Paddings</key>
                <rect>8,8,15,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/mod.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>239,82,477,163</rect>
                <key>scale9Paddings</key>
                <rect>239,82,477,163</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/mod/button.png</key>
            <key type="filename">main/mod/m.png</key>
            <key type="filename">main/mod/m0001.png</key>
            <key type="filename">main/mod/m0002.png</key>
            <key type="filename">main/mod/m0003.png</key>
            <key type="filename">main/mod/m0004.png</key>
            <key type="filename">main/mod/m0005.png</key>
            <key type="filename">main/mod/m0006.png</key>
            <key type="filename">main/mod/m0007.png</key>
            <key type="filename">main/mod/m0008.png</key>
            <key type="filename">main/mod/m0009.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,23,47,47</rect>
                <key>scale9Paddings</key>
                <rect>23,23,47,47</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/news-button-hover.png</key>
            <key type="filename">main/news-button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,21,45,41</rect>
                <key>scale9Paddings</key>
                <rect>23,21,45,41</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/news-new.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>22,12,45,24</rect>
                <key>scale9Paddings</key>
                <rect>22,12,45,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/paperdoll/body.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>66,63,133,127</rect>
                <key>scale9Paddings</key>
                <rect>66,63,133,127</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/paperdoll/paperdoll.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>69,78,138,155</rect>
                <key>scale9Paddings</key>
                <rect>69,78,138,155</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/phone-button-active.png</key>
            <key type="filename">main/phone-button-hover.png</key>
            <key type="filename">main/phone-button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,36,53,71</rect>
                <key>scale9Paddings</key>
                <rect>27,36,53,71</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/sit.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,16,37,31</rect>
                <key>scale9Paddings</key>
                <rect>18,16,37,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/small-box-hover.png</key>
            <key type="filename">main/small-box.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,15,30,30</rect>
                <key>scale9Paddings</key>
                <rect>15,15,30,30</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/snowball-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,7,15,15</rect>
                <key>scale9Paddings</key>
                <rect>7,7,15,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/snowball/ball.png</key>
            <key type="filename">main/snowball/ground.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,6,11,12</rect>
                <key>scale9Paddings</key>
                <rect>6,6,11,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/snowball/shadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,3,9,5</rect>
                <key>scale9Paddings</key>
                <rect>5,3,9,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/spinner.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,7,17,15</rect>
                <key>scale9Paddings</key>
                <rect>8,7,17,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/tab-2.png</key>
            <key type="filename">main/tab.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>52,11,104,22</rect>
                <key>scale9Paddings</key>
                <rect>52,11,104,22</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/tab-arrow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,3,10,6</rect>
                <key>scale9Paddings</key>
                <rect>5,3,10,6</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/wave.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,20,43,41</rect>
                <key>scale9Paddings</key>
                <rect>21,20,43,41</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/x-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,6,13,13</rect>
                <key>scale9Paddings</key>
                <rect>6,6,13,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>main</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array>
            <string>phaser3-exporter-beta</string>
        </array>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties">
            <key>aphrodite::media-query-2x</key>
            <key>css::media-query-2x</key>
            <struct type="ExporterProperty">
                <key>value</key>
                <string>(-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi)</string>
            </struct>
            <key>aphrodite::sprite-prefix</key>
            <key>css::sprite-prefix</key>
            <struct type="ExporterProperty">
                <key>value</key>
                <string></string>
            </struct>
        </map>
    </struct>
</data>
